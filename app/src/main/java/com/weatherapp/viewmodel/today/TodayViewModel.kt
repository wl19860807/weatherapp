package com.weatherapp.viewmodel.today

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.res.Resources
import com.weatherapp.R
import com.weatherapp.data.model.DataModel
import com.weatherapp.until.DateUtil

class TodayViewModel: ViewModel() {
    var twoDaysData = MutableLiveData<MutableList<DataModel>>()
    var currentlyData = MutableLiveData<DataModel>()

    var dateText = MutableLiveData<String>()
    var summaryText = MutableLiveData<String>()
    var tempNowText = MutableLiveData<String>()
    var humidityText = MutableLiveData<String>()
    var pressureText = MutableLiveData<String>()
    var windspeedText = MutableLiveData<String>()
    var visibilityText = MutableLiveData<String>()

    fun updateTodayDataInform(resources: Resources, currentlyDataModel: DataModel) {
        dateText.value = DateUtil.getDateStringByFormat(currentlyDataModel.time, DateUtil.Date_Format_MMMMdHHmm)
        summaryText.value = currentlyDataModel.summary
        tempNowText.value = "${resources.getString(R.string.temperature)} : ${currentlyDataModel.apparentTemperature}"
        humidityText.value = "${resources.getString(R.string.humidity)} : ${currentlyDataModel.humidity}"
        pressureText.value = "${resources.getString(R.string.pressure)} : ${currentlyDataModel.pressure}"
        windspeedText.value = "${resources.getString(R.string.windspeed)} : ${currentlyDataModel.windSpeed}"
        visibilityText.value = "${resources.getString(R.string.visibility)} : ${currentlyDataModel.visibility}"
        currentlyData.value = currentlyDataModel
    }


    fun updateTwoDaysData(hourlyDatas: MutableList<DataModel>) {
        val tomorrowDateTS = DateUtil.getTomorrowDateTS()
        twoDaysData.value = hourlyDatas.asSequence().filter {
            it.time < tomorrowDateTS ||  it.time == tomorrowDateTS
        }.toMutableList()
    }
}