General Requirements:

UX/UI

1. 2 tabs,

 	Tab Today - > currently,  and 24 hours information (hourly , ts filter from currentTs.hour to currentTs.hour + 24)

 	TabComing Days- > daily information

2. Tab Today

	headerView: currently information:

		time to Date,

		summary,

		apparentTemperature,

		humidity, pressure, windSpeed, visibility

	bodyView1 (Horizontal RecyclerView): 24 hourly information, temperature

		FixedHeight: 200dp == 100 temperature  Rate: (2/1)

	bodyViewItem:

		Height: temperature x Rate, eg: 10 temperature x 200/100 == 20dp

		FixedWidth: 20dp

		time

2. Tab Coming Days

	bodyView (Vertical RecyclerView): Daily information, temperature

	bodyViewItem:

		time to Date, summary, temperatureHigh, temperatureLow

3. Date format:

	September 30, 00:00 -> mmmm d, hh:mm

	00:00 -> h:mm

4. Location:

	Runtime Permission

5. Refresh

	ActionButton to refresh weather data

Design Pattern : Android Architecture + MVVM

Dependency:

	Network: Okhttp, Retrofit, Gson
	Design: RecyclerView, Constraint layout
	Location: Google Play Location Services
	Android Architecture: lifecycle,







