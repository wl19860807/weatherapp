package com.weatherapp.view.today

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.weatherapp.R
import com.weatherapp.data.model.DataModel
import com.weatherapp.data.model.WeatherModel
import com.weatherapp.view.today.adapter.UnitAdapter
import com.weatherapp.viewmodel.MainViewModel
import com.weatherapp.viewmodel.today.TodayViewModel
import kotlinx.android.synthetic.main.fragment_today.*

class TodayFragment: Fragment() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var todayViewModel: TodayViewModel
    private var tempAdapter: UnitAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel = ViewModelProviders.of(activity!!).get(MainViewModel::class.java)
        todayViewModel = ViewModelProviders.of(this).get(TodayViewModel::class.java)
        subscribeViewModel(mainViewModel)
        subscribeSubViewModel(todayViewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_today, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        today_temps.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            tempAdapter = UnitAdapter(context)
            adapter = tempAdapter
        }
    }

    private fun subscribeViewModel(viewModel: MainViewModel) {
        viewModel.weatherModel.observe(this, Observer<WeatherModel>{
            it?.hourly?.data?.let { _data ->
                todayViewModel.updateTwoDaysData(_data)
            }
            it?.currently?.let {_data ->
                todayViewModel.updateTodayDataInform(resources, _data)
            }

        })
    }

    private fun subscribeSubViewModel(viewModel: TodayViewModel) {
        viewModel.twoDaysData.observe(this, Observer<MutableList<DataModel>> {
            it?.let { _data ->
                updateTempAdapter(_data)
            }
        })
        viewModel.currentlyData.observe(this, Observer<DataModel>{
            updateTodayInform(viewModel)
        })
    }

    private fun updateTodayInform(viewModel: TodayViewModel) {
        today_header_time.text = viewModel.dateText.value
        today_header_summary.text = viewModel.summaryText.value
        today_header_temp_now.text = viewModel.tempNowText.value
        today_header_humidity.text = viewModel.humidityText.value
        today_header_pressure.text = viewModel.pressureText.value
        today_header_windspeed.text = viewModel.windspeedText.value
        today_header_visibility.text = viewModel.visibilityText.value
    }

    private fun updateTempAdapter(hourlyDatas: MutableList<DataModel>) {
        tempAdapter?.setData(hourlyDatas)
    }
}