package com.weatherapp.view.today.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.view_unit_data_item.view.*

class UnitViewHolder(itemview: View): RecyclerView.ViewHolder(itemview){
    val unitData = itemview.unit_data
    val unitDataVisual = itemview.unit_data_visual
    val unitTime = itemview.unit_time

}