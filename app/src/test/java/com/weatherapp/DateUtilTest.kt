package com.weatherapp

import com.weatherapp.until.DateUtil
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class DateUtilTest {
    @Test
    fun dateFormatTest() {
        val dateTS = 1538345675L
        var dateString1 = DateUtil.getDateStringByFormat(dateTS, DateUtil.Date_Format_HHmm)
        assertEquals("00:14", dateString1)

        dateString1 = DateUtil.getDateStringByFormat(dateTS, DateUtil.Date_Format_MMMMdHHmm)
        assertEquals("October 1, 00:14", dateString1)

        dateString1 = DateUtil.getDateStringByFormat(dateTS, DateUtil.Date_Format_EEEdMMMM)
        assertEquals("Mon, 1 October", dateString1)
    }
}
