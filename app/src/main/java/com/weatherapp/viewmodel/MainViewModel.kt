package com.weatherapp.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import android.view.View
import com.weatherapp.data.model.WeatherModel
import com.weatherapp.data.repository.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel: ViewModel() {
    val weatherModel = MutableLiveData<WeatherModel>()
    val loadingProgressBarVisibility = MutableLiveData<Int>()

    fun getWeatherModel(location: Location) {
        ApiClient.instance.getWeather(location.latitude, location.longitude).enqueue(object: Callback<WeatherModel> {
            override fun onResponse(call: Call<WeatherModel>?, response: Response<WeatherModel>?) {
                response?.body().let { _weatherMode ->
                    weatherModel.value = _weatherMode
                }
                loadingProgressBarVisibility.value = View.GONE
            }

            override fun onFailure(call: Call<WeatherModel>?, t: Throwable?) {
                loadingProgressBarVisibility.value = View.GONE
            }
        })

    }
}