package com.weatherapp.view.today.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weatherapp.R
import com.weatherapp.data.model.DataModel
import com.weatherapp.until.DateUtil
import kotlin.math.roundToInt

class UnitAdapter(context: Context): RecyclerView.Adapter<UnitViewHolder>() {
    private var unitDatas = mutableListOf<DataModel>()
    private val inflater = LayoutInflater.from(context)
    private val heightMax = context.resources.getDimensionPixelOffset(R.dimen.unit_height_max)
    private val tempUnitMax = context.resources.getInteger(R.integer.unit_temp_max)

    fun setData(unitDatas: MutableList<DataModel>) {
        this.unitDatas = unitDatas
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewgroup: ViewGroup, position: Int) = UnitViewHolder(inflater.inflate(R.layout.view_unit_data_item, viewgroup, false))

    override fun onBindViewHolder(holder: UnitViewHolder, position: Int) {
        unitDatas[position].let {
            holder.unitData.text = it.temperature.toString()
            val param = holder.unitDataVisual.layoutParams
            param.height = (it.temperature * heightMax/tempUnitMax).roundToInt()
            holder.unitTime.text = DateUtil.getDateStringByFormat(it.time, DateUtil.Date_Format_HHmm)
        }
    }

    override fun getItemCount() = unitDatas.size
}