package com.weatherapp.view.future.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weatherapp.R
import com.weatherapp.data.model.DataModel
import com.weatherapp.until.DateUtil

class DataAdapter(context: Context): RecyclerView.Adapter<DataViewHolder>() {
    private var datas = mutableListOf<DataModel>()
    private val inflater = LayoutInflater.from(context)
    private val context = context

    fun setData(datas: MutableList<DataModel>) {
        this.datas = datas
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewgroup: ViewGroup, position: Int) = DataViewHolder(inflater.inflate(R.layout.view_future_data_item, viewgroup, false))

    override fun getItemCount() = datas.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        datas[position].let {
            holder.dataDate.text = DateUtil.getDateStringByFormat(it.time, DateUtil.Date_Format_EEEdMMMM)
            holder.dataSummary.text = it.summary
            holder.dataTempMax.text = "${context.getString(R.string.max)} : ${it.temperatureHigh}"
            holder.dataTempMin.text = "${context.getString(R.string.min)} : ${it.temperatureLow}"
        }
    }

}