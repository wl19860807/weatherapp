package com.weatherapp.until

import java.text.SimpleDateFormat
import java.util.*

object DateUtil {
    val Date_Format_HHmm = "HH:mm"
    val Date_Format_MMMMdHHmm = "MMMM d, HH:mm"
    val Date_Format_EEEdMMMM = "EEE, d MMMM"

    val mills = 1000

    fun getDateStringByFormat(ts: Long, format: String): String {
        val cal = Calendar.getInstance(Locale.getDefault())
        cal.timeInMillis = ts * mills
        return SimpleDateFormat(format, Locale.getDefault()).format(cal.time).toString()
    }

    fun getTomorrowDateTS(): Long {
        val cal = Calendar.getInstance(Locale.getDefault())
        cal.apply {
            add(Calendar.DATE, 2)
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        return cal.timeInMillis / mills
    }
}