package com.weatherapp.until

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

object PermissionHelper {
    val LOCATION_MODE_CODE = 1001
    val LOCATION_REQUEST_CODE = 2001
    val Location_Permission = Manifest.permission.ACCESS_FINE_LOCATION

    fun isLocationPermissionGranded(activity: Activity) = ContextCompat.checkSelfPermission(activity, Location_Permission) == PackageManager.PERMISSION_GRANTED

    fun shouldShowReason(activity: Activity) = ActivityCompat.shouldShowRequestPermissionRationale(activity, Location_Permission)

    fun requestLocationPermissions(activity: Activity) {
        ActivityCompat.requestPermissions(activity,
                arrayOf(Location_Permission), PermissionHelper.LOCATION_REQUEST_CODE)
    }

    fun openAppSettings(activity: Activity) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivityForResult(intent, LOCATION_MODE_CODE)
    }
}