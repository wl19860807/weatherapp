package com.weatherapp.data.model

data class WeatherModel(
        val currently: DataModel,
        val hourly: UnitModel,
        val daily: UnitModel
)

data class UnitModel(
        val summary: String,
        val data: MutableList<DataModel>
)

data class DataModel(
        val time: Long,
        val summary: String,
        val temperature: Double,
        val apparentTemperature: Double,
        val humidity: Double,
        val pressure: Double,
        val windSpeed: Double,
        val visibility: Double,
        val temperatureHigh: Double,
        val temperatureLow: Double
)
