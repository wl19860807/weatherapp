package com.weatherapp.view.future.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.view_future_data_item.view.*

class DataViewHolder(itemview: View): RecyclerView.ViewHolder(itemview) {
    val dataDate = itemview.data_date
    val dataSummary = itemview.data_summary
    val dataTempMax = itemview.data_temp_max
    val dataTempMin = itemview.data_temp_min
}