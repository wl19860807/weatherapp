package com.weatherapp.data.repository

import com.weatherapp.data.model.WeatherModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiServices {
    @GET("/forecast/2bb07c3bece89caf533ac9a5d23d8417/{lat},{lon}")
    fun getWeather(@Path("lat", encoded = true) lat: Double, @Path("lon", encoded = true) lon: Double): Call<WeatherModel>

}