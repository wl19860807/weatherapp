package com.weatherapp.view.future

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.weatherapp.R
import com.weatherapp.data.model.DataModel
import com.weatherapp.data.model.WeatherModel
import com.weatherapp.view.future.adapter.DataAdapter
import com.weatherapp.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_future.*

class FutureFragment: Fragment() {
    private lateinit var mainViewModel: MainViewModel
    private var dataAdapter: DataAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel = ViewModelProviders.of(activity!!).get(MainViewModel::class.java)
        subscribeViewModel(mainViewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_future, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        future_data.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            dataAdapter = DataAdapter(context)
            adapter = dataAdapter
        }
    }

    private fun subscribeViewModel(viewModel: MainViewModel) {
        viewModel.weatherModel.observe(this, Observer<WeatherModel>{
            it?.daily?.data?.let { _data ->
                updateTempAdapter(_data)
            }
        })
    }

    private fun updateTempAdapter(dailyDatas: MutableList<DataModel>) {
        dataAdapter?.setData(dailyDatas)
    }
}