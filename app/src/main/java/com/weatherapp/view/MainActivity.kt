package com.weatherapp.view

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.weatherapp.R
import com.weatherapp.until.PermissionHelper
import com.weatherapp.view.future.FutureFragment
import com.weatherapp.view.today.TodayFragment
import com.weatherapp.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var viewpagerAdapter: ViewPagerAdapter
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        subscribeViewModel(mainViewModel)
        initUI()
    }

    override fun onStart() {
        super.onStart()
        if (PermissionHelper.isLocationPermissionGranded(this)) {
            checkLocation()
        } else {
            PermissionHelper.requestLocationPermissions(this)
        }
    }

    private fun subscribeViewModel(viewModel: MainViewModel) {
        viewModel.loadingProgressBarVisibility.observe(this, Observer<Int>{
            progressbar.visibility = it?: View.VISIBLE
        })
    }

    private fun initUI() {
        initViewPager()
        initTabs()

        location_btn.setOnClickListener {
            if (PermissionHelper.isLocationPermissionGranded(this)) {
                checkLocation()
            } else {
                PermissionHelper.requestLocationPermissions(this)
            }
        }
    }
    private fun initTabs() {
        tab_layout.setupWithViewPager(viewpager)
    }

    private fun initViewPager() {
        viewpagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewpagerAdapter.addFragment(TodayFragment(), getString(R.string.today))
        viewpagerAdapter.addFragment(FutureFragment(), getString(R.string.future))

        viewpager.adapter = viewpagerAdapter
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PermissionHelper.LOCATION_MODE_CODE -> {
                if (PermissionHelper.isLocationPermissionGranded(this)) {
                    checkLocation()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionHelper.LOCATION_REQUEST_CODE -> {
                when {
                    grantResults.contains(PackageManager.PERMISSION_GRANTED) -> checkLocation()
                    PermissionHelper.shouldShowReason(this) -> {
                        Snackbar.make(viewpager, R.string.permission_location_reason_msg, Snackbar.LENGTH_INDEFINITE)
                                .setAction(R.string.ok) {
                                    PermissionHelper.requestLocationPermissions(this)
                                }
                                .show()
                    }
                    else -> Snackbar.make(viewpager, R.string.permission_location_denied_permantaly_msg, Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok) {
                                PermissionHelper.openAppSettings(this)
                            }
                            .show()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun checkLocation() {
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    location?.let {
                        mainViewModel.getWeatherModel(it)
                    }
                }
    }
}
